# Kino Backend Doku

API Dokumentation für das Kinosystem Backend

# Anmerkungen zum Backend
- Die Youtube Links zu den Trailern sind im Movie Dokument im Feld "trailer" als string gespeichert.
- Die Json Web Tokens (JWT) laufen nach 4 Stunden aus. Der User muss sich dann neu einloggen.
- **User sind nicht das gleiche wie Customer**. User sind Angestellte des Kinos, die z.B. das Ticket invalidieren. **Customer sind die Nutzer, die Tickets buchen, für die Kinobesucher müssen also auch die Customer Routes verwendet werden** (/register erzeugt automatisch einen Customer)
- Tickets, die länger als 15 Minuten im Warenkorb verbleiben, werden automatisch aus dem Warenkorb entfernt und wieder als verfügbar markiert
- Einkaufswagen werden nach 10 Tagen ohne Aktivität automatisch gelöscht
- Der Buchungsprozess ist entweder /ticket/select => /cart/checkout/book oder /ticket/select => /cart/checkout/reserve => /ticket/pay (im Nachhinein)
- Die cart/checkout/* routes entfernen die Tickets aus dem Einkaufswagen. Eingeloggte Customer können ihre gebuchten und reservierten Tickets im Nachhinein über /ticket/me einsehen.

